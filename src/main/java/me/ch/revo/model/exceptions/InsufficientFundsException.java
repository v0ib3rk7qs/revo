package me.ch.revo.model.exceptions;

public class InsufficientFundsException extends OperationException {

    public InsufficientFundsException() {
        super("insufficient funds to perform operation");
    }

    @Override
    public int statusCode() {
        return 402;
    }
}
