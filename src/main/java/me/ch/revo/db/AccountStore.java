package me.ch.revo.db;

import io.vavr.control.Try;
import me.ch.revo.db.jooq.tables.pojos.Account;
import me.ch.revo.db.jooq.tables.pojos.AccountBalanceChange;
import me.ch.revo.model.AccountInfo;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface AccountStore {

    Try<AccountInfo> createAccount(String name);

    // of course, normally such query wouldn't make sense, but for development it's useful
    Try<List<AccountInfo>> getAccounts();

    // Optional means account was not found
    Try<AccountInfo> getAccountInfo(Account account);

    Try<Account> getAccount(UUID id);

    // TODO: return some representation of the transfer
    Try<AccountBalanceChange> deposit(Account account, BigDecimal amount);

    Try<Optional<AccountBalanceChange>> getTransaction(Account account, long txId);

    Try<List<AccountBalanceChange>> accountTransactions(Account account);

    Try<AccountBalanceChange> transfer(Account from, Account to, BigDecimal amount);

    default void shutdown() {};
}
