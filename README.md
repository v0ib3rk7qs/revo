#### Requirements ####
Docker, JDK 11+

#### Building ####
Either `$ ./mvnw clean verify -U` or `$ ./dockerbuild.sh`

#### Running ####
- `$ docker run -it --rm revolut`
- `$ target/revoluttest-1.0-SNAPSHOT/revoluttest-1.0-SNAPSHOT/bin/start.sh`
- `$ java -jar target/revoluttest-1.0-SNAPSHOT-fat.jar`

#### Configuration ####
Uses [Typesafe config](https://github.com/lightbend/config),
defaults in `src/main/resources/reference.conf`, env overrides in `src/main/application.conf`.

#### Testing ####
Integration tests run required postgres image themselves,
for manual testing there's a Postman collection in `postman/postman.json`.

#### API & Docs ####
`docs/api.html` contains API doc derived from Postman collection.
To regenerate, use [postmanerator](https://github.com/aubm/postmanerator):

```shell
$ postmanerator -collection postman/postman.json -output docs/api.html
```

#### Tech & Potential Improvements ####
- Based on [Spark microframework](https://github.com/perwendel/spark).
    Unfortunately, it provides zero support for asynchronous execution,
    requiring a large thread pool to get any serious performance.
    
- Performance was sacrificed for code simplicity almost everywhere - for example,
  there's no need to fetch entire account info for most of the calls,
  except for making `AccountStore` API somewhat cleaner.

- Event sourcing implementation is very barebones and unoptimized -
    some periodic compaction/checkpointing of account balances required to get better performance.

- Accounts don't even have any event lineage, just simple and incomplete CRUD.

- No simple way to find the cause of a balance change.

- Error handling and constraints are somewhat inconsistently spread between app and database.
    Database doesn't have checks for quite a lot of edge cases,
    e.g. check for transfers within the same account is only in the application.
    
- Test suite is very minimal, mostly happy paths, some secondary endpoints have no tests, e.g. transactions.

