package me.ch.revo.http;

import com.fasterxml.jackson.databind.JsonNode;
import me.ch.revo.AppConfig;
import me.ch.revo.db.AccountStore;
import me.ch.revo.db.PostgresAccountStore;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;

import java.math.BigDecimal;
import java.net.http.HttpResponse;
import java.util.UUID;

abstract class AbstractApiIT {

    protected static AppConfig config;

    protected static Api          api;
    protected static AccountStore store;

    @BeforeAll
    static void launchApi() {
        config = AppConfig.load();

        store = new PostgresAccountStore(config.getPostgres());
        api = new Api(config.getExecution(), config.getHttpServer(), store);

        api.start();
    }

    @AfterAll
    static void shutdownApi() {
        api.shutdown();
        store.shutdown();
    }


    public UUID createAccount() {
        final var accountName = UUID.randomUUID().toString();


        final var res = post("/accounts?name=" + accountName);

        return UUID.fromString(res.body().path("id").textValue());
    }

    public UUID createAccount(final BigDecimal balance) {
        final var accountId = createAccount();

        post(String.format("/accounts/%s/deposit?amount=%s", accountId, balance.toString()));

        return accountId;
    }

    public JsonNode getAccount(final UUID id) {
        return get("/accounts/" + id).body();
    }

    public HttpResponse<JsonNode> get(final String path) {
        return TestUtils.get(config.getHttpServer(), path);

    }

    public HttpResponse<JsonNode> post(final String path) {
        return TestUtils.post(config.getHttpServer(), path);

    }

}
