package me.ch.revo.model.exceptions;

public class NotFoundException extends OperationException {

    public NotFoundException(final String subject) {
        super(subject + " not found");
    }

    @Override
    public int statusCode() {
        return 400;
    }
}
