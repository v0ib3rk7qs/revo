#!/usr/bin/env bash

WHEREAMI=`dirname "${0}"`
if [ -z "${APP_ROOT}" ]; then
    export APP_ROOT=`cd "${WHEREAMI}/../" && pwd`
fi


APPLIB="${APP_ROOT}/lib"

java ${JAVA_OPTS} -jar ${APPLIB}/revoluttest-1.0-SNAPSHOT-fat.jar
