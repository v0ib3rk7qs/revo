package me.ch.revo.http;

import java.util.Objects;

public class HttpConfig {

    private String host;
    private int    port;

    public HttpConfig() {
    }

    public HttpConfig(final String host, final int port) {
        this.host = host;
        this.port = port;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    @Override
    public String toString() {
        return "HttpConfig{" +
               "host='" + host + '\'' +
               ", port=" + port +
               '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HttpConfig that = (HttpConfig) o;
        return port == that.port &&
               Objects.equals(host, that.host);
    }

    @Override
    public int hashCode() {
        return Objects.hash(host, port);
    }
}
