package me.ch.revo.model.exceptions;

public abstract class OperationException extends RuntimeException {

    public OperationException(final String message) {
        super(message);
    }

    public abstract int statusCode();

}
