package me.ch.revo.http;

import io.vavr.control.Try;
import me.ch.revo.AppConfig.ExecutionConfig;
import me.ch.revo.db.AccountStore;
import me.ch.revo.db.jooq.tables.pojos.Account;
import me.ch.revo.db.jooq.tables.pojos.AccountBalanceChange;
import me.ch.revo.model.AccountInfo;
import me.ch.revo.model.exceptions.InvalidParametersException;
import me.ch.revo.model.exceptions.MissingParameterException;
import me.ch.revo.model.exceptions.NotFoundException;
import me.ch.revo.model.exceptions.OperationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;
import spark.Spark;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.function.Function;

import static io.vavr.API.*;
import static io.vavr.Predicates.instanceOf;
import static java.util.Objects.requireNonNull;
import static spark.Spark.*;

public class Api {

    private static final Logger LOG = LoggerFactory.getLogger(Api.class);

    private final HttpConfig      httpConfig;
    private final AccountStore    store;
    private final ExecutionConfig executionConfig;

    public Api(final ExecutionConfig executionConfig,
               final HttpConfig httpConfig,
               final AccountStore store) {

        this.executionConfig = requireNonNull(executionConfig, "executionConfig must be not null");
        this.httpConfig = requireNonNull(httpConfig, "httpConfig must be not null");
        this.store = requireNonNull(store, "store must be not null");

    }

    public void start() {

        configureServer();

        setupErrorHandling();

        setupAccountRoutes();

        awaitInitialization();

    }

    public void shutdown() {
        stop();
        awaitStop();
    }

    private void setupErrorHandling() {
        Spark.notFound(JsonTransformer.transformer().render(new ErrorMessage("not found")));

        exception(OperationException.class,
                  (exception, request, response) -> {
                      response.status(exception.statusCode());
                      response.type("application/json");
                      response.body(JsonTransformer.transformer().render(new ErrorMessage(exception.getMessage())));
                  });

        exception(SQLException.class,
                  (exception, request, response) -> {
                      response.status(500);
                      response.type("application/json");
                      response.body(JsonTransformer.transformer().render(new ErrorMessage("database error")));
                  });

        internalServerError((req, res) -> {
            res.type("application/json");
            return "{\"error\":\"internal server error\"}";
        });
    }

    private void setupAccountRoutes() {
        before((request, response) -> response.type("application/json"));

        path("/accounts", () -> {

            Spark.post("", this::createAccount, JsonTransformer.transformer());

            Spark.get("", (req, res) -> getAccounts(), JsonTransformer.transformer());

            Spark.get("/:accountId", (req, res) -> getAccountInfo(req), JsonTransformer.transformer());


            Spark.post("/:accountId/deposit", this::deposit, JsonTransformer.transformer());


            Spark.get("/:accountId/transactions", (req, res) -> listTransactions(req.params(":accountId")), JsonTransformer.transformer());

            Spark.get("/:accountId/transactions/:transactionId",
                      (req, res) -> getTransaction(req.params(":accountId"), req.params(":transactionId")),
                      JsonTransformer.transformer());

            Spark.post("/:accountId/transfers",
                       this::transfer,
                       JsonTransformer.transformer());

        });
    }

    private void configureServer() {

        threadPool(executionConfig.getMaxThreads(), executionConfig.getMinThreads(),
                   (int) executionConfig.getIdleTimeout().toMillis());

        ipAddress(httpConfig.getHost());
        port(httpConfig.getPort());

        threadPool(16, 4, 30000); // TODO: have this in httpConfig

        initExceptionHandler(ex -> {
            LOG.error("fatal error on startup, exiting", ex);
            System.exit(100);
        });

        before("/*", (req, res) -> LOG.trace("received request: {}", req.uri()));
        after((req, res) -> LOG.trace("response: {}", res.body()));
    }

    private Try<AccountInfo> createAccount(final Request req, final Response res) {

        return Option(req.queryParams("name"))
                .toTry(() -> new InvalidParametersException("missing name"))
                .flatMap(store::createAccount)
                .onSuccess(account -> {
                    res.status(201);
                    res.header("Location", URLUtils.subPath(req, account.getId()));
                });
    }

    private List<AccountInfo> getAccounts() {
        return store.getAccounts().get();
    }

    private Try<AccountInfo> getAccountInfo(final Request req) {
        return getAccount(req.params(":accountId")).flatMap(store::getAccountInfo);

    }

    private Try<AccountBalanceChange> deposit(final Request req, final Response res) {

        final var account = getAccount(req.params(":accountId"));

        final var amount = parseAmount(req.queryParams("amount"));

        return For(account, amount)
                .yield(store::deposit)
                .flatMap(Function.identity())

                .onSuccess(createdStatus(req, res));
    }

    private Try<AccountBalanceChange> transfer(final Request req, final Response res) {
        final var amount = parseAmount(req.queryParams("amount"));

        final var targetAcc = getAccount(req.queryParams("to"));
        final var sourceAcc = getAccount(req.params(":accountId"));


        return For(sourceAcc, targetAcc, amount)
                .yield(store::transfer)
                .flatMap(Function.identity())

                .onSuccess(createdStatus(req, res));
    }

    private Try<Account> getAccount(final String rawId) {
        return parseAccountId(rawId).flatMap(store::getAccount);
    }

    @SuppressWarnings("unchecked") // unfortunately, vavr's mapFailure causes this, no way around
    private Try<UUID> parseAccountId(final String raw) {

        return Option(raw)
                .toTry(() -> new MissingParameterException("account id"))
                .map(UUID::fromString)
                .mapFailure(Case($(instanceOf(IllegalArgumentException.class)),
                                 ex -> new InvalidParametersException("malformed account id")));

    }

    @SuppressWarnings("unchecked")
    private Try<Long> getTxId(final String raw) {
        return Option(raw)
                .toTry(() -> new MissingParameterException("missing transaction id"))
                .map(Long::parseLong)

                .mapFailure(Case($(instanceOf(NumberFormatException.class)),
                                 ex -> new InvalidParametersException("invalid transaction id: " + ex.getMessage())));
    }

    @SuppressWarnings("unchecked")
    private Try<BigDecimal> parseAmount(final String raw) {

        // TODO: force exact %.2f format. Would require NumberFormat, which is not thread-safe
        return Option(raw)
                .toTry(() -> new MissingParameterException("amount"))

                .map(BigDecimal::new)
                .mapFailure(Case($(instanceOf(NumberFormatException.class)),
                                 ex -> new InvalidParametersException("invalid amount: " + ex.getMessage())))

                .filter(am -> am.signum() != -1, () -> new InvalidParametersException("negative amount"))
                .filter(am -> am.signum() != 0,  () -> new InvalidParametersException("zero not allowed"));
    }


    private Try<List<AccountBalanceChange>> listTransactions(final String rawAccountId) {
        return getAccount(rawAccountId)
                .flatMap(store::accountTransactions);
    }

    private Try<AccountBalanceChange> getTransaction(final String rawAccountId, final String rawTxId) {

        return For(getAccount(rawAccountId), getTxId(rawTxId))

                .yield(store::getTransaction)
                .flatMap(Function.identity())

                .mapTry(o -> o.orElseThrow(() -> new NotFoundException("transaction")));
    }

    private Consumer<AccountBalanceChange> createdStatus(final Request req, final Response res) {
        return tx -> {
            res.status(201);
            res.header("Location", URLUtils.stripLast(req.pathInfo()) + URLUtils.uri("/transactions", tx.getId()));
        };
    }
}
