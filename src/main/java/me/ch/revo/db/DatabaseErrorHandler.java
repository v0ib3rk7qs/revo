package me.ch.revo.db;

import me.ch.revo.model.exceptions.InsufficientFundsException;
import org.jooq.ExecuteContext;
import org.jooq.impl.DefaultExecuteListener;

public class DatabaseErrorHandler extends DefaultExecuteListener {

    public static final String INSUFFICIENT_FUNDS_SQL_STATE = "2POOR";

    @Override
    public void exception(final ExecuteContext ctx) {
        if (INSUFFICIENT_FUNDS_SQL_STATE.equals(ctx.sqlException().getSQLState())) {
            ctx.exception(new InsufficientFundsException());
        }
    }
}
