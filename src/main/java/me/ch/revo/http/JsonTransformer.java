package me.ch.revo.http;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import io.vavr.Value;
import spark.ResponseTransformer;

class JsonTransformer implements ResponseTransformer {

    private static final ObjectMapper om = new ObjectMapper()
            .findAndRegisterModules()
            .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

    private static JsonTransformer INSTANCE = new JsonTransformer();

    private JsonTransformer() {
    }

    static JsonTransformer transformer() {
        return INSTANCE;
    }

    @Override
    public String render(final Object model) {

        if (model == null) {
            return null;
        }

        try {
            if (model instanceof Value) { // automatically unwrap vavr values
                return om.writeValueAsString(((Value) model).get());
            }

            return om.writeValueAsString(model);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("serialization exception");
        }
    }

}
