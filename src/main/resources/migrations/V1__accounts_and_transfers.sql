create table account (
  -- autoincremented id is faster and easier to work with, but opens up some potential security issues
  id         UUID          not null,
  name       varchar(4096) not null,
  created_at timestamptz   not null default now(),

  primary key (id),

  constraint name_not_empty check (trim(name) != '')
);

create table account_event (

  -- it's important to have an autoincrementing pk - it provides quick scans in roughly chronological order
  -- maybe later could be replaced by a composite (timestamp, deduplication_id)
  id         bigint      not null generated always as identity,
  timestamp  timestamptz not null default now(),
  account_id UUID        not null references account (id),

  primary key (id)
);

create index accountIdx
  on account_event (account_id);

-- TODO: maybe add a link to cause?
create table account_balance_change (
  like account_event including all,
  change decimal(20, 2) not null
)
  inherits (account_event);

create or replace function check_positive_balance()
  returns trigger as
$$
begin
  if ((select coalesce(sum(change), 0) from account_balance_change where account_id = new.account_id) + new.change < 0)
  then
    RAISE exception SQLSTATE '2POOR'
    using message = 'resulting balance cannot be negative';
  end if;
  return new;
end
$$
language 'plpgsql';

create trigger "require_positive_balance"
  before insert
  on account_balance_change
  for row execute procedure check_positive_balance();


create view current_balance as (select acc.id as account_id, coalesce(sum(updates.change), 0) as balance
                                from account acc
                                       left join account_balance_change updates on acc.id = updates.account_id

                                group by acc.id);

-- TODO: add more checks and constraints, so that you can't transfer negative amounts,
-- initiating.change + counterparty.change = 0 etc.
-- can't make transfers inherit balance changes, unfortunately, since they belong to two accounts
create table fund_transfer (
  initiating   bigint not null references account_balance_change (id),
  counterparty bigint not null references account_balance_change (id)
);


create table deposit (
  like account_balance_change including all,

  constraint change_must_be_positive check (change > 0)
)
  inherits (account_balance_change);
