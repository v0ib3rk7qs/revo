package me.ch.revo.http;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandler;
import java.net.http.HttpResponse.BodyHandlers;
import java.net.http.HttpResponse.BodySubscribers;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.ThreadLocalRandom;

import static io.vavr.API.unchecked;

public class TestUtils {

    public static final ObjectMapper om = new ObjectMapper().findAndRegisterModules();

    private static final HttpClient client = HttpClient.newHttpClient();


    public static HttpResponse<JsonNode> get(final HttpConfig serverConf, final String path) {

        try {
            return client.send(HttpRequest.newBuilder()
                                          .GET()
                                          .uri(subjectUri(serverConf).resolve(path))
                                          .build(), jsonHandler());
        } catch (final IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }

    }

    // TODO: refactor this copy-paste
    public static HttpResponse<JsonNode> post(final HttpConfig serverConf, final String path) {

        try {
            return client.send(HttpRequest.newBuilder()
                                          .POST(HttpRequest.BodyPublishers.noBody())
                                          .uri(subjectUri(serverConf).resolve(path))
                                          .build(), jsonHandler());
        } catch (final IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }

    }

    public static URI subjectUri(final HttpConfig config) {
        return URI.create(String.format("http://%s:%d", config.getHost(), config.getPort()));
    }

    private static BodyHandler<JsonNode> jsonHandler() {

        final var stringSub = BodySubscribers.ofString(StandardCharsets.UTF_8);
        final var mappedSub = BodySubscribers.mapping(stringSub, unchecked(om::readTree));

        return BodyHandlers.fromSubscriber(mappedSub, m -> m.getBody().toCompletableFuture().join());

    }

    public static BigDecimal randomMoneyAmount() {
        return BigDecimal.valueOf(ThreadLocalRandom.current().nextDouble(5, 1000))
                         .setScale(2, RoundingMode.DOWN)
                         .stripTrailingZeros();
    }
}
