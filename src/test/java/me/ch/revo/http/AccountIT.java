package me.ch.revo.http;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class AccountIT extends AbstractApiIT {

    @Test
    @DisplayName("account creation")
    void accountCreation() {

        final var accountName = UUID.randomUUID().toString();

        final var res = post("/accounts?name=" + accountName);
        assertEquals(201, res.statusCode());

        final var body = res.body();
        assertEquals(accountName, body.path("name").textValue());
        assertTrue(body.has("id"));

        final var id = body.get("id").textValue();

        assertEquals(Optional.of("/accounts/" + id), res.headers().firstValue("Location"));
    }

    @Test
    @DisplayName("account fetching")
    void accountFetching() {

        final var accId = createAccount();

        final var res = get("/accounts/" + accId.toString());
        assertEquals(200, res.statusCode());

        final var body = res.body();
        assertEquals(accId.toString(), body.path("id").textValue());
        assertEquals(0, body.path("balance").intValue());

    }

}
