#!/usr/bin/env bash -e

./mvnw clean verify -U

docker build . -t revolut
