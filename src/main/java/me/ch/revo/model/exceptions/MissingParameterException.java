package me.ch.revo.model.exceptions;

public class MissingParameterException extends InvalidParametersException {

    public MissingParameterException(final String parameterName) {
        super(parameterName + " missing");
    }
}
