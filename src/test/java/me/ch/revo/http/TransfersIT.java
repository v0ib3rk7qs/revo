package me.ch.revo.http;

import com.fasterxml.jackson.databind.JsonNode;
import io.vavr.CheckedFunction3;
import io.vavr.Tuple3;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.http.HttpResponse;
import java.util.UUID;

import static io.vavr.API.Tuple;
import static me.ch.revo.http.TestUtils.randomMoneyAmount;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TransfersIT extends AbstractApiIT {

    @Test
    @DisplayName("successful transfer")
    void testSuccessfulTransfer() {

        final var subject = transferSubjects();

        checkTransfer(subject._1, subject._2, subject._3);

    }

    @Test
    @DisplayName("multiple transfers")
    void testMultipleTransfers() {

        final var subject = transferSubjects();

        final var amount = subject._3.divide(BigDecimal.valueOf(3), RoundingMode.DOWN);

        checkTransfer(subject._1, subject._2, amount);
        checkTransfer(subject._1, subject._2, amount);

    }

    @Test
    @DisplayName("transfer if balance < amount")
    void testInsufficientFundsTransfer() {

        assertingRollback((source, dest, balance) -> {

            expectResult(402,
                         "{\"error\":\"insufficient funds to perform operation\"}",
                         transfer(source, dest, balance.add(BigDecimal.valueOf(0.01))));

            return null;
        });

    }

    @Test
    @DisplayName("transfer of negative amount")
    void testZeroTransfer() {

        assertingRollback((source, dest, balance) -> {

            expectResult(400,
                         "{\"error\":\"invalid arguments: zero not allowed\"}",
                         transfer(source, dest, BigDecimal.ZERO));

            return null;
        });

    }

    @Test
    @DisplayName("transfer of negative amount")
    void testNegativeAmountTransfer() {

        assertingRollback((source, dest, balance) -> {

            expectResult(400,
                         "{\"error\":\"invalid arguments: negative amount\"}",
                         transfer(source, dest, BigDecimal.ONE.negate()));

            return null;
        });

    }


    @Test
    @DisplayName("transfer to same account")
    void testTransferToSameAccount() {

        assertingRollback((source, dest, balance) -> {

            expectResult(400,
                         "{\"error\":\"account cannot be a target of this operation: destination cannot be the same as source\"}",
                         transfer(source, source, balance));

            return null;
        });

    }

    @Test
    @DisplayName("transfer from non-existing account")
    void testTransferFromNonExistingAccount() {
        assertAccountNotFound(transfer(UUID.randomUUID(), createAccount(), BigDecimal.TEN));
    }

    @Test
    @DisplayName("transfer to non-existing account")
    void testTransferToNonExistingAccount() {
        assertAccountNotFound(transfer(createAccount(BigDecimal.TEN), UUID.randomUUID(), BigDecimal.TEN));
    }

    @Test
    @DisplayName("transfer without amount")
    void testTransferWithoutAmount() {

        assertingRollback((source, dest, balance) -> {

            expectResult(400,
                         "{\"error\":\"invalid arguments: amount missing\"}",
                         post(String.format("/accounts/%s/transfers?to=%s", source, dest)));

            return null;
        });


    }

    @Test
    @DisplayName("transfer with malformed amount")
    void testTransferWithMalformedAmount() {

        assertingRollback((source, dest, balance) -> {

            expectResult(400,
                         "{\"error\":\"invalid arguments: invalid amount: " +
                         "Character a is neither a decimal digit number, " +
                         "decimal point, nor \\\"e\\\" notation exponential mark.\"}",
                         post(String.format("/accounts/%s/transfers?to=%s&amount=abc", source, dest)));

            return null;
        });

    }

    @Test
    @DisplayName("transfer without destination")
    void testTransferWithoutDestination() {

        assertingRollback((source, dest, balance) -> {

            expectResult(400,
                         "{\"error\":\"invalid arguments: account id missing\"}",
                         post(String.format("/accounts/%s/transfers?amount=%s", source, balance)));

            return null;
        });


    }

    private <T> T assertingRollback(final CheckedFunction3<UUID, UUID, BigDecimal, T> test) {

        final var subject = transferSubjects();

        final var res = test.tupled().unchecked().apply(subject);

        final var resultingSourceBalance = getBalance(subject._1);
        final var resultingDestBalance   = getBalance(subject._2);

        assertEquals(subject._3, resultingSourceBalance);
        assertEquals(BigDecimal.ZERO, resultingDestBalance);

        return res;

    }

    private void checkTransfer(final UUID source,
                               final UUID dest,
                               final BigDecimal amount) {


        final var sourceBalance = getBalance(source);
        final var destBalance   = getBalance(dest);

        final var res = transfer(source, dest, amount);

        assertEquals(amount.negate().stripTrailingZeros(), res.body().path("change").decimalValue().stripTrailingZeros());

        final var expectedTxLocationPattern = String.format("/accounts/%s/transactions/\\d+", source);

        assertTrue(res.headers().firstValue("Location")
                      .map(header -> header.matches(expectedTxLocationPattern))
                      .orElse(false));

        assertEquals(sourceBalance.subtract(amount).stripTrailingZeros(), getBalance(source));

        assertEquals(destBalance.add(amount).stripTrailingZeros(), getBalance(dest));

    }

    private void assertAccountNotFound(final HttpResponse<JsonNode> res) {
        expectResult(400, "{\"error\":\"account not found\"}", res);
    }

    private void expectResult(final int expectedStatus, final String expectedPayload,
                              final HttpResponse<JsonNode> res) {
        assertEquals(expectedStatus, res.statusCode());
        assertEquals(expectedPayload, res.body().toString());
    }

    private Tuple3<UUID, UUID, BigDecimal> transferSubjects() {
        final var balance = randomMoneyAmount();
        return Tuple(createAccount(balance), createAccount(), balance);
    }

    private HttpResponse<JsonNode> transfer(final UUID source,
                                            final UUID dest,
                                            final BigDecimal transferAmount) {
        return post(String.format("/accounts/%s/transfers?to=%s&amount=%s", source, dest, transferAmount));
    }

    private BigDecimal getBalance(final UUID source) {
        return getAccount(source).path("balance").decimalValue().stripTrailingZeros();
    }
}
