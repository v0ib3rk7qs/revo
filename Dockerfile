FROM openjdk:11.0.3-slim


RUN mkdir /revolut


COPY target/revoluttest-1.0-SNAPSHOT/revoluttest-1.0-SNAPSHOT /revolut

RUN useradd microbank && chown -R microbank /revolut

WORKDIR /revolut

ENV JAVA_OPTS="-XX:MaxRAMPercentage=60"

RUN touch /tmp/access.log

EXPOSE 8080


ENTRYPOINT ["sh", "-c", "bin/start.sh"]
