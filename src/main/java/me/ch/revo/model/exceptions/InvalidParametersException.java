package me.ch.revo.model.exceptions;

public class InvalidParametersException extends OperationException {

    public InvalidParametersException(final String message) {
        super("invalid arguments: " + message);
    }

    @Override
    public int statusCode() {
        return 400;
    }
}
