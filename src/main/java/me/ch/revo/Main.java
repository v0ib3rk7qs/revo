package me.ch.revo;

import me.ch.revo.db.PostgresAccountStore;
import me.ch.revo.http.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {


    private static final Logger LOG = LoggerFactory.getLogger(Main.class);


    public static void main(final String... args) {

        final var config = AppConfig.load();
        System.out.println(config);


        final var store = new PostgresAccountStore(config.getPostgres());
        final var api   = new Api(config.getExecution(), config.getHttpServer(), store);

        api.start();
        LOG.info("started");

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            api.shutdown();
            store.shutdown();
            LOG.info("stopped");
        }));


    }


}
