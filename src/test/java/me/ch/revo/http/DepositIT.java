package me.ch.revo.http;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.net.URLEncoder;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class DepositIT extends AbstractApiIT {

    @Test
    @DisplayName("depositing and updating balance")
    public void testDeposit() {

        final var accId = createAccount();

        assertEquals(0, getAccount(accId).path("balance").intValue()); // just in case, to be sure it's actually zero

        final var amount = TestUtils.randomMoneyAmount();
        final var res = post("/accounts/" + accId + "/deposit?amount=" + URLEncoder.encode(amount.toString(), UTF_8));
        System.out.println(res);

        assertEquals(201, res.statusCode());

        final var body = res.body();

        assertEquals(amount, body.path("change").decimalValue());


        assertEquals(amount, getAccount(accId).path("balance").decimalValue());

    }
}
