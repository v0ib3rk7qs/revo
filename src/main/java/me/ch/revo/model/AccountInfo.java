package me.ch.revo.model;

import me.ch.revo.db.jooq.tables.pojos.Account;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.UUID;

import static java.util.Objects.requireNonNull;

public class AccountInfo {

    private final UUID           id;
    private final String         name;
    private final OffsetDateTime createdAt;

    private final BigDecimal balance;

    public AccountInfo(final UUID id, final String name, final OffsetDateTime createdAt, final BigDecimal balance) {
        this.id = requireNonNull(id, "id must be not null ");
        this.name = requireNonNull(name, "name must be not null ");
        this.createdAt = requireNonNull(createdAt, "createdAt must be not null ");
        this.balance = requireNonNull(balance, "balance must be not null");
    }

    public static AccountInfo fromDatabase(final Account account, final BigDecimal balance) {
        return new AccountInfo(account.getId(), account.getName(), account.getCreatedAt(), balance);
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public OffsetDateTime getCreatedAt() {
        return createdAt;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    @Override
    public String toString() {
        return "AccountInfo{" +
               "id=" + id +
               ", name='" + name + '\'' +
               ", createdAt=" + createdAt +
               ", balance=" + balance +
               '}';
    }
}
