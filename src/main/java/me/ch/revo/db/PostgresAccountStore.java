package me.ch.revo.db;

import com.zaxxer.hikari.HikariDataSource;
import io.vavr.CheckedFunction1;
import io.vavr.control.Try;
import me.ch.revo.db.jooq.Revolut;
import me.ch.revo.db.jooq.tables.pojos.Account;
import me.ch.revo.db.jooq.tables.pojos.AccountBalanceChange;
import me.ch.revo.db.jooq.tables.records.AccountRecord;
import me.ch.revo.model.AccountInfo;
import me.ch.revo.model.exceptions.IneligibleTargetAccount;
import me.ch.revo.model.exceptions.InvalidParametersException;
import me.ch.revo.model.exceptions.NotFoundException;
import org.jooq.*;
import org.jooq.conf.RenderMapping;
import org.jooq.conf.Settings;
import org.jooq.impl.DSL;
import org.jooq.impl.DefaultConfiguration;
import org.jooq.impl.DefaultExecuteListenerProvider;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static java.util.Objects.requireNonNull;
import static me.ch.revo.db.jooq.Tables.*;

public class PostgresAccountStore implements AccountStore {

    private final HikariDataSource dataSource;

    public PostgresAccountStore(final PostgresConfig config) {

        requireNonNull(config, "config must be not null");

        dataSource = HikariUtils.configure(config);

        FlywayMigrations.migrate(dataSource, config.getSchema());

    }

    @Override
    public Try<AccountInfo> createAccount(final String name) {

        return withDsl(dsl -> {

            final var rec = new AccountRecord()
                    .setId(UUID.randomUUID())
                    .setName(name);

            return dsl.insertInto(ACCOUNT)
                      .set(rec)
                      .returning()
                      .fetchOne()
                      .map(r -> AccountInfo.fromDatabase(r.into(Account.class), BigDecimal.ZERO)); // TODO: proper mapper

        });

    }

    @Override
    public Try<List<AccountInfo>> getAccounts() {
        return withDsl(dsl -> accountQuery(dsl).fetch().map(r -> r.into(AccountInfo.class)));
    }

    @Override
    public Try<AccountInfo> getAccountInfo(final Account account) {
        return withDsl(dsl -> AccountInfo.fromDatabase(account, getBalance(dsl, account)));
    }

    @Override
    public Try<Account> getAccount(final UUID id) {

        return withDsl(dsl -> dsl.selectFrom(ACCOUNT)
                                 .where(ACCOUNT.ID.eq(id))
                                 .fetchOptionalInto(Account.class)
                                 .orElseThrow(() -> new NotFoundException("account")));
    }

    @Override
    public Try<AccountBalanceChange> deposit(final Account account, final BigDecimal amount) {

        return withDsl(dsl ->
                               dsl.insertInto(DEPOSIT)
                                  .set(DEPOSIT.ACCOUNT_ID, account.getId())
                                  .set(DEPOSIT.CHANGE, amount)
                                  .returning()
                                  .fetchOne()
                                  .into(AccountBalanceChange.class));
    }

    @Override
    public Try<Optional<AccountBalanceChange>> getTransaction(final Account account, final long txId) {

        return withDsl(dsl ->
                               dsl.selectFrom(ACCOUNT_BALANCE_CHANGE)
                                  .where(ACCOUNT_BALANCE_CHANGE.ACCOUNT_ID.eq(account.getId()),
                                         ACCOUNT_BALANCE_CHANGE.ID.eq(txId))
                                  .fetchOptionalInto(AccountBalanceChange.class));
    }

    @Override
    public Try<List<AccountBalanceChange>> accountTransactions(final Account account) {

        return withDsl(dsl ->
                               dsl.selectFrom(ACCOUNT_BALANCE_CHANGE)
                                  .where(ACCOUNT_BALANCE_CHANGE.ACCOUNT_ID.eq(account.getId()))
                                  .orderBy(ACCOUNT_BALANCE_CHANGE.TIMESTAMP.desc())
                                  .fetchInto(AccountBalanceChange.class));
    }

    @Override
    public Try<AccountBalanceChange> transfer(final Account from, final Account to, final BigDecimal amount) {

        return withDsl(dsl -> {

            if (from.getId().equals(to.getId())) {
                throw new IneligibleTargetAccount("destination cannot be the same as source");
            }

            // since we're doing event sourcing and postgres doesn't support predicate locks, we need some separate lock
            // to prevent concurrent withdrawals/transfers from account
            // in this case, account row with `SELECT ... FOR UPDATE`
            dsl.selectFrom(ACCOUNT)
               .where(ACCOUNT.ID.eq(from.getId()))
               .forUpdate()
               .fetchOne();

            if (amount.signum() < 1) { // just in case, normally shouln't even get to this
                throw new InvalidParametersException("positive amount required");
            }

            final var balanceChanges =
                    dsl.insertInto(ACCOUNT_BALANCE_CHANGE, ACCOUNT_BALANCE_CHANGE.ACCOUNT_ID, ACCOUNT_BALANCE_CHANGE.CHANGE)
                       .values(from.getId(), amount.negate())
                       .values(to.getId(), amount)
                       .returning()
                       .fetch()
                       .intoGroups(ACCOUNT_BALANCE_CHANGE.ACCOUNT_ID);


            final var sourceChange = balanceChanges.get(from.getId()).get(0);
            final var targetChange = balanceChanges.get(to.getId()).get(0);

            dsl.insertInto(FUND_TRANSFER, FUND_TRANSFER.INITIATING, FUND_TRANSFER.COUNTERPARTY)
               .values(sourceChange.getId(), targetChange.getId())
               .execute();

            return sourceChange.into(AccountBalanceChange.class);

        });

    }

    @Override
    public void shutdown() {
        dataSource.close();
    }

    private BigDecimal getBalance(final DSLContext dsl, final Account account) {
        return dsl
                .select(DSL.sum(ACCOUNT_BALANCE_CHANGE.CHANGE))
                .from(ACCOUNT_BALANCE_CHANGE)
                .where(ACCOUNT_BALANCE_CHANGE.ACCOUNT_ID.eq(account.getId()))

                .fetchOptional()
                .map(Record1::value1)
                .orElse(BigDecimal.ZERO);
    }

    private SelectJoinStep<Record> accountQuery(final DSLContext dsl) { // TODO: could dsl requirement be removed w/ static DSL?

        final var table = ACCOUNT.leftJoin(CURRENT_BALANCE)
                                 .on(ACCOUNT.ID.eq(CURRENT_BALANCE.ACCOUNT_ID));
        return dsl
                .select(ACCOUNT.fields())
                .select(CURRENT_BALANCE.BALANCE)

                .from(table);
    }

    private <T> Try<T> withDsl(final CheckedFunction1<DSLContext, T> operation) {
        return withDsl(operation, Connection.TRANSACTION_REPEATABLE_READ);
    }

    private <T> Try<T> withDsl(final CheckedFunction1<DSLContext, T> operation,
                               final int isolationLevel) {

        return Try.withResources(dataSource::getConnection)
                  .of(conn -> {
                      conn.setAutoCommit(false);
                      conn.setTransactionIsolation(isolationLevel);

                      try {
                          final var r = operation.apply(dsl(conn));
                          conn.commit();
                          return r;
                      } catch (final Throwable ex) {
                          conn.rollback();
                          throw ex;
                      }
                  });
    }

    private static DSLContext dsl(final Connection conn) {

        final var renderMapping = new RenderMapping()
                .withDefaultSchema(Revolut.REVOLUT.getName());

        final var settings = new Settings().withRenderMapping(renderMapping);

        final var dslConf = new DefaultConfiguration()
                .set(new DefaultExecuteListenerProvider(new DatabaseErrorHandler()))
                .set(SQLDialect.POSTGRES)
                .set(settings)
                .set(conn);

        return DSL.using(dslConf);

    }

}
