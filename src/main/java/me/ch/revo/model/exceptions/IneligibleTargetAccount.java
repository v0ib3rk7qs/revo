package me.ch.revo.model.exceptions;

public class IneligibleTargetAccount extends OperationException {

    public IneligibleTargetAccount(final String message) {
        super("account cannot be a target of this operation: " + message);
    }

    @Override
    public int statusCode() {
        return 400;
    }


}
