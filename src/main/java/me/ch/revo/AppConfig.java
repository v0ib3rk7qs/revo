package me.ch.revo;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigBeanFactory;
import com.typesafe.config.ConfigFactory;
import me.ch.revo.db.PostgresConfig;
import me.ch.revo.http.HttpConfig;

import java.time.Duration;

import static java.util.Objects.requireNonNull;

public class AppConfig { // has to be a javabean for mapping to work

    private ExecutionConfig execution;

    private HttpConfig     httpServer;
    private PostgresConfig postgres;

    public AppConfig() {
    }

    public AppConfig(final ExecutionConfig execution, final HttpConfig httpServer, final PostgresConfig postgres) {
        this.execution = requireNonNull(execution);
        this.httpServer = requireNonNull(httpServer);
        this.postgres = requireNonNull(postgres);
    }

    public static AppConfig load() {
        return parse(ConfigFactory.load());
    }

    public static AppConfig parse(final Config config) {
        return ConfigBeanFactory.create(config, AppConfig.class);
    }

    public HttpConfig getHttpServer() {
        return httpServer;
    }

    public void setHttpServer(HttpConfig httpServer) {
        this.httpServer = httpServer;
    }

    public PostgresConfig getPostgres() {
        return postgres;
    }

    public void setPostgres(PostgresConfig postgres) {
        this.postgres = postgres;
    }

    public ExecutionConfig getExecution() {
        return execution;
    }

    public void setExecution(final ExecutionConfig execution) {
        this.execution = execution;
    }

    public static class ExecutionConfig {

        private int      minThreads;
        private int      maxThreads;
        private Duration idleTimeout;

        public ExecutionConfig() {
        }

        public ExecutionConfig(final int minThreads, final int maxThreads, final Duration idleTimeout) {
            this.minThreads = minThreads;
            this.maxThreads = maxThreads;
            this.idleTimeout = idleTimeout;
        }

        public int getMinThreads() {
            return minThreads;
        }

        public void setMinThreads(final int minThreads) {
            this.minThreads = minThreads;
        }

        public int getMaxThreads() {
            return maxThreads;
        }

        public void setMaxThreads(final int maxThreads) {
            this.maxThreads = maxThreads;
        }

        public Duration getIdleTimeout() {
            return idleTimeout;
        }

        public void setIdleTimeout(final Duration idleTimeout) {
            this.idleTimeout = idleTimeout;
        }

        @Override
        public String toString() {
            return "ExecutionConfig{" +
                   "minThreads=" + minThreads +
                   ", maxThreads=" + maxThreads +
                   ", idleTimeout=" + idleTimeout +
                   '}';
        }
    }

    @Override
    public String toString() {
        return "AppConfig{" +
               "httpServer=" + httpServer +
               ", postgres=" + postgres +
               '}';
    }
}
