package me.ch.revo.db;

import org.flywaydb.core.Flyway;

import javax.sql.DataSource;

class FlywayMigrations {

    private FlywayMigrations() {
        throw new UnsupportedOperationException();
    }

    public static void migrate(final DataSource ds, final String schema) {

        Flyway.configure()
              .dataSource(ds)
              .schemas(schema)
              .locations("classpath:migrations")
              .load()
              .migrate();
    }
}
