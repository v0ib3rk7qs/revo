package me.ch.revo.http;

import com.fasterxml.jackson.annotation.JsonProperty;

import static java.util.Objects.requireNonNull;

public class ErrorMessage {

    private final String message;

    public ErrorMessage(final String message) {
        this.message = requireNonNull(message, "message must be not null");
    }

    @JsonProperty("error")
    public String getMessage() {
        return message;
    }
}

