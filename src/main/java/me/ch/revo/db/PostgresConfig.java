package me.ch.revo.db;

import com.typesafe.config.Optional;

import java.time.Duration;
import java.util.Objects;

public class PostgresConfig {

    private String  host;

    @Optional
    private Integer port;

    private String database;

    private String schema;

    private String dataSourceClass;

    private String user, password;

    private Duration connectionTimeout, idleTimeout;

    private Duration maxLifetime;

    private int minIdleConnections, maxPoolSize;

    public PostgresConfig() {
    }

    public PostgresConfig(final String host, final int port,
                          final String database, final String schema,
                          final String dataSourceClass,
                          final String user, final String password,
                          final Duration connectionTimeout, final Duration idleTimeout, final Duration maxLifetime,
                          final int minIdleConnections, final int maxPoolSize) {
        this.host = host;
        this.port = port;
        this.database = database;
        this.schema = schema;
        this.dataSourceClass = dataSourceClass;
        this.user = user;
        this.password = password;
        this.connectionTimeout = connectionTimeout;
        this.idleTimeout = idleTimeout;
        this.maxLifetime = maxLifetime;
        this.minIdleConnections = minIdleConnections;
        this.maxPoolSize = maxPoolSize;
    }

    public String getHost() {
        return host;
    }

    public void setHost(final String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public String getDatabase() {
        return database;
    }

    public void setDatabase(final String database) {
        this.database = database;
    }

    public void setPort(final int port) {
        this.port = port;
    }

    public String getSchema() {
        return schema;
    }

    public void setSchema(final String schema) {
        this.schema = schema;
    }

    public String getDataSourceClass() {
        return dataSourceClass;
    }

    public void setDataSourceClass(final String dataSourceClass) {
        this.dataSourceClass = dataSourceClass;
    }

    public String getUser() {
        return user;
    }

    public void setUser(final String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public Duration getConnectionTimeout() {
        return connectionTimeout;
    }

    public void setConnectionTimeout(final Duration connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
    }

    public Duration getIdleTimeout() {
        return idleTimeout;
    }

    public void setIdleTimeout(final Duration idleTimeout) {
        this.idleTimeout = idleTimeout;
    }

    public Duration getMaxLifetime() {
        return maxLifetime;
    }

    public void setMaxLifetime(final Duration maxLifetime) {
        this.maxLifetime = maxLifetime;
    }

    public int getMinIdleConnections() {
        return minIdleConnections;
    }

    public void setMinIdleConnections(final int minIdleConnections) {
        this.minIdleConnections = minIdleConnections;
    }

    public int getMaxPoolSize() {
        return maxPoolSize;
    }

    public void setMaxPoolSize(final int maxPoolSize) {
        this.maxPoolSize = maxPoolSize;
    }

    @Override
    public String toString() {
        return "PostgresConfig{" +
               "host='" + host + '\'' +
               ", port=" + port +
               ", database='" + database + '\'' +
               ", schema='" + schema + '\'' +
               ", dataSourceClass='" + dataSourceClass + '\'' +
               ", user='" + user + '\'' +
               ", password='" + password + '\'' +
               ", connectionTimeout=" + connectionTimeout +
               ", idleTimeout=" + idleTimeout +
               ", maxLifetime=" + maxLifetime +
               ", minIdleConnections=" + minIdleConnections +
               ", maxPoolSize=" + maxPoolSize +
               '}';
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final PostgresConfig that = (PostgresConfig) o;
        return port == that.port &&
               minIdleConnections == that.minIdleConnections &&
               maxPoolSize == that.maxPoolSize &&
               Objects.equals(host, that.host) &&
               Objects.equals(database, that.database) &&
               Objects.equals(schema, that.schema) &&
               Objects.equals(dataSourceClass, that.dataSourceClass) &&
               Objects.equals(user, that.user) &&
               Objects.equals(password, that.password) &&
               Objects.equals(connectionTimeout, that.connectionTimeout) &&
               Objects.equals(idleTimeout, that.idleTimeout) &&
               Objects.equals(maxLifetime, that.maxLifetime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(host, port, database, schema, dataSourceClass, user, password, connectionTimeout, idleTimeout, maxLifetime, minIdleConnections, maxPoolSize);
    }
}
