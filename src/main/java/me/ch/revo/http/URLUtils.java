package me.ch.revo.http;

import spark.Request;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.StringJoiner;

final class URLUtils {

    private URLUtils() {
        throw new UnsupportedOperationException();
    }

    static String subPath(final Request req, final Object... segments) {
        return uri(req.pathInfo(), segments);
    }

    static String uri(final Object first, Object... rest) {

        final var joiner = new StringJoiner("/")
                .add(first.toString());

        for (final var section : rest) {

            Objects.requireNonNull(section, "null segment");
            try {
                joiner.add(URLEncoder.encode(section.toString(), StandardCharsets.UTF_8.name()));
            } catch (final UnsupportedEncodingException e) {
                throw new RuntimeException(e); // will never happen
            }
        }

        return joiner.toString();

    }

    static String stripLast(final String uri) {

        final var i = uri.lastIndexOf("/");

        if (i == -1) return uri;

        return uri.substring(0, i);
    }
}
