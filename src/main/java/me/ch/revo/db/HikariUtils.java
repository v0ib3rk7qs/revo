package me.ch.revo.db;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

final class HikariUtils {

    private HikariUtils() {
        throw new UnsupportedOperationException();
    }

    static HikariDataSource configure(final PostgresConfig config) {
        final var hikariConf = new HikariConfig();

        hikariConf.setDataSourceClassName(config.getDataSourceClass());

        hikariConf.addDataSourceProperty("serverName", config.getHost());
        hikariConf.addDataSourceProperty("portNumber", config.getPort());

        hikariConf.addDataSourceProperty("databaseName", config.getDatabase());
        hikariConf.addDataSourceProperty("currentSchema", config.getSchema());

        hikariConf.addDataSourceProperty("user", config.getUser());

        if (config.getPassword() != null) {
            hikariConf.addDataSourceProperty("password", config.getPort());
        }

        hikariConf.setMinimumIdle(config.getMinIdleConnections());
        hikariConf.setMaximumPoolSize(config.getMaxPoolSize());

        hikariConf.setIdleTimeout(config.getIdleTimeout().toMillis());
        hikariConf.setMaxLifetime(config.getMaxLifetime().toMillis());

        hikariConf.setConnectionTimeout(config.getConnectionTimeout().toMillis());


        return new HikariDataSource(hikariConf);

    }
}
